package gradeclassifier

import cats.data.ValidatedNel
import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import io.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s.circe._
import cats.implicits._
import gradeclassifier.model._
import org.apache.spark.ml.PipelineStage
import org.apache.spark.ml.classification.{DecisionTreeClassifier, GBTClassifier, MultilayerPerceptronClassifier, RandomForestClassifier}
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeBuilder
import java.io.File
import scala.concurrent.duration.DurationConversions.Classifier

object GradeServer {

  val service: HttpService[IO] = HttpService[IO] {
    case GET -> Root / "correlation" / first /second =>
      SparkJobs.correlationJob(first, second).flatMap { corr =>
        corr.map(d => Ok(d.asJson.noSpaces)).getOrElse(BadRequest())
      }
    case req @ POST -> Root / "classification" / classifier =>
      handleClassification(req, classifier)

    case req @ GET -> Root / "test" =>
      SparkJobs.sparkJobTest *> Ok("asd")

    case request @ GET -> Root / file =>
      StaticFile.fromFile(new File(file), Some(request))
        .getOrElseF(NotFound())

  }
  implicit val userDecoder = jsonOf[IO, Payload]

  def handleClassification(req: Request[IO], classifier: String): IO[Response[IO]] =
    parseClassifier(classifier)
      .fold(nel => BadRequest(nel.mkString("\n")), c => for {
        payload <- req.as[Payload]
        result <- SparkJobs.classifierJob(c)(payload.study.ensureNonZero.toDigit, payload.course)
        resp <- Ok(result.asJson.noSpaces)
      } yield resp)

  def parseClassifier(c: String): ValidatedNel[String, PipelineStage] = (c.toLowerCase match {
    case "mlp" => {

      // create the trainer and set its parameters
      new MultilayerPerceptronClassifier()
        .setBlockSize(128)
        .setSeed(1234L)
        .setMaxIter(100)
        .setFeaturesCol("features")
        .setLabelCol("label")
        .validNel
    }
    case "dt" => new DecisionTreeClassifier()
      .setFeaturesCol("features")
      .setLabelCol("label")
      .validNel
    case "gbt" => new GBTClassifier()
      .setFeaturesCol("features")
      .setLabelCol("label")
      .validNel
    case "rf" => new RandomForestClassifier()
      .setFeaturesCol("features")
      .setLabelCol("label")
      .validNel
    case _ => "Could not parse classifier".invalidNel
  })

  val builder: IO[Server[IO]] = BlazeBuilder[IO].bindHttp(8080, "localhost")
    .mountService(service, "/api").start
}
