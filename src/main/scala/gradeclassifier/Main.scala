package gradeclassifier

import cats.effect._
import fs2.{Stream, StreamApp}

import fs2.StreamApp.ExitCode
import scala.concurrent.ExecutionContext.Implicits.global
import org.http4s.server.blaze._

object Main extends StreamApp[IO] {
  override def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, ExitCode] =
    BlazeBuilder[IO]
      .bindHttp(9000, "localhost")
      .mountService(GradeServer.service, "/")
      .serve
}
