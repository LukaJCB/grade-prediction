package gradeclassifier

import eu.timepit.refined._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.Interval.Closed

case class Grade(points: Int Refined Closed[W.`0`.T, W.`100`.T]) {
  def normalized: Double = points.value / 100
}

case class Student(grades: Map[Subject, Grade]) extends AnyVal

class Subject(val subject: String) extends AnyVal

