package gradeclassifier

import cats.effect.IO
import gradeclassifier.model._
import cats.implicits._
import org.apache.spark.ml.classification.{DecisionTreeClassifier, MultilayerPerceptronClassifier, RandomForestClassifier}
import org.apache.spark.ml.evaluation.MulticlassClassificationEvaluator
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import gradeclassifier.util.groupWhile
import org.apache.spark.ml.{Pipeline, PipelineModel, PipelineStage}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.mllib.stat.Statistics
import org.apache.spark.rdd.RDD
import org.apache.spark.sql
import org.http4s.server.blaze.BlazeBuilder

object SparkJobs {

  def classifierJob(classifier: PipelineStage)(p: Study, course: String): IO[Double] = IO {
    val spark = SparkSession
      .builder
      .appName("MultilayerPerceptronClassifierExample")
      .config("spark.master", "local")
      .getOrCreate()


    import spark.implicits._

    val data = spark.read.csv("data_bmi.csv")
      .collect()
      .toVector
      .tail
      .map(Entry.fromRow)
      .collect { case Right(r) => r }


    val people = Person.fromEntries(data)


    val label = "label"

    val moduleList =
      List("objektorientierteProgrammierung1", "objektorientierteProgrammierung2", "webProgrammierung", "mediengestaltung1", "mediengestaltung2", "technischesEnglisch", "mathematik1", "mathematik2", "mathematik3", "datenbanksysteme1", "datenbanksysteme2", "formaleModelleUndAlgorithmen", "softwareEngineering", "rechnerarchitektur", "computergrafik1", "menschComputerInteraktion", "webEngineering", "rechnernetze", "digitaleBildUndTontechnik", "grundlagenDerBetriebswirtschaft", "itSicherheit", "kommunikationsDesign", "projektmanagementUndItRecht")


    val modules = moduleList.map(s => if (s == course) label else s)


    val filtered = modules.filter(p.hasValue)
    val features = filtered.filter(_ != label)

    val studyFiltered = moduleList.filter(s => p.hasValue(s) || s == course)

    val studies = people.map(Study.fromPerson).filter(_.enoughValues(studyFiltered))

    val dataset: Dataset[Study] = spark.sqlContext.createDataset(studies)

    val layers = Array(features.length, 15, 16, 11)

    val newClassifier =
      if (classifier.isInstanceOf[MultilayerPerceptronClassifier])
        classifier.asInstanceOf[MultilayerPerceptronClassifier].setLayers(layers)
      else
        classifier

    val assembler = new VectorAssembler()
      .setInputCols(features.toArray)
      .setOutputCol("features")


    val pipeline = new Pipeline().setStages(Array(assembler, newClassifier))

    val df = if (studies.nonEmpty) modules.foldLeft(dataset.toDF(modules: _*)) { (acc, cur) =>
      if (!filtered.contains(cur)) acc.drop(cur)
      else acc
    } else dataset.toDF(modules: _*).na.fill(Study.gradeToDigit(300))



    val model: PipelineModel = pipeline.fit(df)


    val test =
      spark.sqlContext.createDataset(Array(p)).toDF(modules: _*).na.fill(Study.gradeToDigit(260))

    val result = model.transform(test)



    val predictionAndLabels = result.select("prediction", label)
    val res = predictionAndLabels.map(_.getDouble(0)).collect().apply(0)

    spark.stop()

    Study.digitToGrade(res.toInt).toDouble / 100

  }

  def correlationJob(first: String, second: String): IO[Option[Double]] = IO {
    val spark = SparkSession
      .builder
      .appName("MultilayerPerceptronClassifierExample")
      .config("spark.master", "local")
      .getOrCreate()

    import spark.implicits._

    val data = spark.read.csv("data.csv")
      .collect()
      .toVector
      .tail
      .map(Entry.fromRow)
      .collect { case Right(r) => r }

    val people = Person.fromEntries(data)

    val studies = people.map(Study.fromPerson)


    def correlation(xs: Seq[(Int, Int)]): Double = {
      val (lefts, rights) = xs.unzip
      println(lefts)
      println(rights)
      val toRdd = (vec: Seq[Int]) => spark.sparkContext.makeRDD(vec.map(_.toDouble))
      Statistics.corr(toRdd(lefts), toRdd(rights))
    }

    val result =
      Entry.findCorrelation(people.map(_.entries), first.replace("%20", " "), second.replace("%20", " "), correlation)

    spark.stop()

    result
  }

  val sparkJobTest: IO[Unit] = IO {

    val spark = SparkSession
      .builder
      .appName("MultilayerPerceptronClassifierExample")
      .config("spark.master", "local")
      .getOrCreate()


    import spark.implicits._

    val data = spark.read.csv("data_bmi.csv")
      .collect()
      .toVector
      .tail
      .map(Entry.fromRow)
      .collect { case Right(r) => r }


    val people = Person.fromEntries(data)

    val studies = people.map(Study.fromPerson)


    val dataset: Dataset[Study] = spark.sqlContext.createDataset(studies)



    val label = "label"

    val modules =
      Array("objektorientierteProgrammierung1", "objektorientierteProgrammierung2", "webProgrammierung", "mediengestaltung1", "mediengestaltung2", "technischesEnglisch", "mathematik1", "mathematik2", "mathematik3", "datenbanksysteme1", "datenbanksysteme2", "formaleModelleUndAlgorithmen", "softwareEngineering", "rechnerarchitektur", "computergrafik1", "menschComputerInteraktion", "webEngineering", "rechnernetze", "digitaleBildUndTontechnik", "grundlagenDerBetriebswirtschaft", label, "kommunikationsDesign", "projektmanagementUndItRecht")
    val features = modules.filter(_ != label)

    val layers = Array(features.length, 15, 16, 11)

    val assembler = new VectorAssembler()
      .setInputCols(features)
      .setOutputCol("features")

    val trainer = new MultilayerPerceptronClassifier()
      .setLayers(layers)
      .setBlockSize(128)
      .setSeed(1234L)
      .setMaxIter(100)
      .setFeaturesCol("features")
      .setLabelCol("label")

    val pipeline = new Pipeline().setStages(Array(assembler, trainer))

    val df = dataset.toDF(modules: _*).na.fill(Study.gradeToDigit(260))

    val splits = df.randomSplit(Array(0.6, 0.4), seed = 1234L)
    val train = splits(0)
    val test = splits(1)

    val model = pipeline.fit(train)

    val result = model.transform(test)


    val predictionAndLabels = result.select("prediction", label)

    //predictionAndLabels.show(5)
    println(predictionAndLabels.map(_.getDouble(0)).collect())

    println("How long is my thing? It's this long: " + df.collect().length)
    val evaluator = new MulticlassClassificationEvaluator()
      .setMetricName("accuracy")

    println("Test set accuracy = " + evaluator.evaluate(predictionAndLabels))

    spark.stop()

  }

}
