document.querySelector("#btn-correlation").onclick = function() {
  const first = document.querySelector("#correlation-first").value
  const second = document.querySelector("#correlation-second").value

  console.log(first, second)

  fetch("/correlation/" + first + "/" + second)
    .then(resp => resp.text().then(res => {
       console.log(res)
       document.querySelector("#correlation-result").innerHTML = res
    }))
}

document.querySelector("#predict-which").onchange = function() {


  [].slice.call(document.querySelectorAll("#grades input")).forEach((elem) => elem.disabled = false)

  const course = document.querySelector("#predict-which").value


  const selected = document.querySelector("#" + course)
  selected.value = ""
  selected.disabled = true
}

document.querySelector("#btn-pred").onclick = function() {
  const grades = [].slice.call(document.querySelectorAll("#grades input"))
    .reduce((acc, cur) => { acc[cur.id] = Number(cur.value); return acc; }, {})

  const method = document.querySelector("#method").value
  document.querySelector("#pred-label").hidden = true
  document.querySelector("#result").hidden = true

  const course = document.querySelector("#predict-which").value

  fetch("/classification/" + method, { body : JSON.stringify({ study: grades, course: course }), method : "POST" })
    .then(resp => resp.text().then(res => {
      document.querySelector("#pred-label").hidden = false
      document.querySelector("#result").hidden = false
      console.log(res)
      document.querySelector("#result").innerHTML = res
    }))
}
