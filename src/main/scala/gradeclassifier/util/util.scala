package gradeclassifier

import cats.data.NonEmptyList

package object util {

  def groupWhile[A](vector: Vector[A])(f: A => Boolean): Vector[NonEmptyList[A]] =
    vector.foldLeft(Vector.empty[NonEmptyList[A]]) { (acc, cur) =>
      val nelCur = NonEmptyList.one(cur)
      if (f(cur)) acc.dropRight(1) ++ Vector(acc.lastOption.map(_.concat(nelCur)).getOrElse(nelCur))
      else acc :+ nelCur
    }

}
