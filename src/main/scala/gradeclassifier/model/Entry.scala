package gradeclassifier.model

import gradeclassifier.model.Study._
import org.apache.spark.sql.{Encoder, Encoders, Row}

import scala.util.Try
import cats.implicits._
import gradeclassifier.util.groupWhile


case class Person(gender: String, entries: List[Entry])

object Person {
  val separator = "10"

  def fromEntries(es: Vector[Entry]): Vector[Person] =
    groupWhile(es)(_.pnr != separator)
      .map(list => Person(list.head.gender, list.filter(Entry.filterEntry)))


}

case class Entry(broken: Boolean,
                 gender: String,
                 status: String,
                 pnr: String,
                 examName: String,
                 account: String,
                 examStatus: String,
                 rescission: String,
                 date: String,
                 points: Int,
                 grade: Option[Int],
                 endDate: String,
                 earlierGrade: Option[Double],
                 earlierDegree: String)

case class Payload(study: Study, course: String)

case class Study(objektorientierteProgrammierung1: Option[Int],
                 objektorientierteProgrammierung2: Option[Int],
                 webProgrammierung: Option[Int],
                 mediengestaltung1: Option[Int],
                 mediengestaltung2: Option[Int],
                 technischesEnglisch: Option[Int],
                 mathematik1: Option[Int],
                 mathematik2: Option[Int],
                 mathematik3: Option[Int],
                 datenbanksysteme1: Option[Int],
                 datenbanksysteme2: Option[Int],
                 formaleModelleUndAlgorithmen: Option[Int],
                 softwareEngineering: Option[Int],
                 rechnerarchitektur: Option[Int],
                 computergrafik1: Option[Int],
                 menschComputerInteraktion: Option[Int],
                 webEngineering: Option[Int],
                 rechnernetze: Option[Int],
                 digitaleBildUndTontechnik: Option[Int],
                 grundlagenDerBetriebswirtschaft: Option[Int],
                 itSicherheit: Option[Int],
                 kommunikationsDesign: Option[Int],
                 projektmanagementUndItRecht: Option[Int]) {
  def toDigit: Study = Study(
    objektorientierteProgrammierung1.map(n => gradeToDigit(pointsToGrade(n))),
    objektorientierteProgrammierung2.map(n => gradeToDigit(pointsToGrade(n))),
    webProgrammierung.map(n => gradeToDigit(pointsToGrade(n))),
    mediengestaltung1.map(n => gradeToDigit(pointsToGrade(n))),
    mediengestaltung2.map(n => gradeToDigit(pointsToGrade(n))),
    technischesEnglisch.map(n => gradeToDigit(pointsToGrade(n))),
    mathematik1.map(n => gradeToDigit(pointsToGrade(n))),
    mathematik2.map(n => gradeToDigit(pointsToGrade(n))),
    mathematik3.map(n => gradeToDigit(pointsToGrade(n))),
    datenbanksysteme1.map(n => gradeToDigit(pointsToGrade(n))),
    datenbanksysteme2.map(n => gradeToDigit(pointsToGrade(n))),
    formaleModelleUndAlgorithmen.map(n => gradeToDigit(pointsToGrade(n))),
    softwareEngineering.map(n => gradeToDigit(pointsToGrade(n))),
    rechnerarchitektur.map(n => gradeToDigit(pointsToGrade(n))),
    computergrafik1.map(n => gradeToDigit(pointsToGrade(n))),
    menschComputerInteraktion.map(n => gradeToDigit(pointsToGrade(n))),
    webEngineering.map(n => gradeToDigit(pointsToGrade(n))),
    rechnernetze.map(n => gradeToDigit(pointsToGrade(n))),
    digitaleBildUndTontechnik.map(n => gradeToDigit(pointsToGrade(n))),
    grundlagenDerBetriebswirtschaft.map(n => gradeToDigit(pointsToGrade(n))),
    itSicherheit.map(n => gradeToDigit(pointsToGrade(n))),
    kommunikationsDesign.map(n => gradeToDigit(pointsToGrade(n))),
    projektmanagementUndItRecht.map(n => gradeToDigit(pointsToGrade(n)))
  )

  def ensureNonZero: Study = Study(
    objektorientierteProgrammierung1.ensure(())(_ != 0),
    objektorientierteProgrammierung2.ensure(())(_ != 0),
    webProgrammierung.ensure(())(_ != 0),
    mediengestaltung1.ensure(())(_ != 0),
    mediengestaltung2.ensure(())(_ != 0),
    technischesEnglisch.ensure(())(_ != 0),
    mathematik1.ensure(())(_ != 0),
    mathematik2.ensure(())(_ != 0),
    mathematik3.ensure(())(_ != 0),
    datenbanksysteme1.ensure(())(_ != 0),
    datenbanksysteme2.ensure(())(_ != 0),
    formaleModelleUndAlgorithmen.ensure(())(_ != 0),
    softwareEngineering.ensure(())(_ != 0),
    rechnerarchitektur.ensure(())(_ != 0),
    computergrafik1.ensure(())(_ != 0),
    menschComputerInteraktion.ensure(())(_ != 0),
    webEngineering.ensure(())(_ != 0),
    rechnernetze.ensure(())(_ != 0),
    digitaleBildUndTontechnik.ensure(())(_ != 0),
    grundlagenDerBetriebswirtschaft.ensure(())(_ != 0),
    itSicherheit.ensure(())(_ != 0),
    kommunikationsDesign.ensure(())(_ != 0),
    projektmanagementUndItRecht.ensure(())(_ != 0)
  )

  def enoughValues(ls: List[String]): Boolean =
    ls.forall(hasValue)
  
  def hasValue(s: String) = s match {
    case "objektorientierteProgrammierung1" =>
      objektorientierteProgrammierung1.nonEmpty
    case "objektorientierteProgrammierung2" =>
      objektorientierteProgrammierung2.nonEmpty
    case "webProgrammierung" =>
      webProgrammierung.nonEmpty
    case "mediengestaltung1" =>
      mediengestaltung1.nonEmpty
    case "mediengestaltung2" =>
      mediengestaltung2.nonEmpty
    case "technischesEnglisch" =>
      technischesEnglisch.nonEmpty
    case "mathematik1" =>
      mathematik1.nonEmpty
    case "mathematik2" =>
      mathematik2.nonEmpty
    case "mathematik3" =>
      mathematik3.nonEmpty
    case "datenbanksysteme1" =>
      datenbanksysteme1.nonEmpty
    case "datenbanksysteme2" =>
      datenbanksysteme2.nonEmpty
    case "formaleModelleUndAlgorithmen" =>
      formaleModelleUndAlgorithmen.nonEmpty
    case "softwareEngineering" =>
      softwareEngineering.nonEmpty
    case "rechnerarchitektur" =>
      rechnerarchitektur.nonEmpty
    case "computergrafik1" =>
      computergrafik1.nonEmpty
    case "menschComputerInteraktion" =>
      menschComputerInteraktion.nonEmpty
    case "webEngineering" =>
      webEngineering.nonEmpty
    case "rechnernetze" =>
      rechnernetze.nonEmpty
    case "digitaleBildUndTontechnik" =>
      digitaleBildUndTontechnik.nonEmpty
    case "grundlagenDerBetriebswirtschaft" =>
      grundlagenDerBetriebswirtschaft.nonEmpty
    case "itSicherheit" =>
      itSicherheit.nonEmpty
    case "kommunikationsDesign" =>
      kommunikationsDesign.nonEmpty
    case "projektmanagementUndItRecht" =>
      projektmanagementUndItRecht.nonEmpty
    case _ => true
  }
}

object Study {

  def fromPersonTransform(p: Person)(f: Int => Int) = Study(
    p.entries.find(_.pnr == "3010").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3070").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3030").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2115").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2116").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2120").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2100").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2101").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2102").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2111").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2112").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3140").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3130").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3090").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2124").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3170").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3220").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3190").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3230").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3240").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3260").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "2131").flatMap(_.grade.map(f)),
    p.entries.find(_.pnr == "3310").flatMap(_.grade.map(f))
  )

  def fromPerson(p: Person): Study = fromPersonTransform(p)(gradeToDigit)

  def gradeToDigit(grade: Int): Int = grade match {
    case 100 => 0
    case 130 => 1
    case 160 | 170 => 2
    case 200 => 3
    case 230 => 4
    case 260 | 270 => 5
    case 300 => 6
    case 330 => 7
    case 360 | 370 => 8
    case 400 => 9
    case _ => 5
  }

  def pointsToGrade(points: Int): Int =
    if (points > 94) 100
    else if (points > 89) 130
    else if (points > 84) 170
    else if (points > 79) 200
    else if (points > 74) 230
    else if (points > 69) 270
    else if (points > 64) 300
    else if (points > 59) 330
    else if (points > 54) 370
    else if (points > 49) 400
    else 500


  def digitToGrade(digit: Int): Int = digit match {
    case 0 => 100
    case 1 => 130
    case 2 => 160
    case 3 => 200
    case 4 => 230
    case 5 => 260
    case 6 => 300
    case 7 => 330
    case 8 => 360
    case 9 => 400
  }

}

object Entry {

  implicit def entryEncoder: Encoder[Entry] = Encoders.product[Entry]

  def fromRow(row: Row): Either[Throwable, Entry] = Try {
    Entry(
      broken = false,
      row.getString(0),
      row.getString(1),
      row.getString(2),
      row.getString(3),
      row.getString(4),
      row.getString(5),
      row.getString(6),
      row.getString(7),
      row.getString(8).toInt,
      if (row.isNullAt(9) || row.getString(9).isEmpty) None else Some(row.getString(9).toInt),
      row.getString(10),
      if (row.isNullAt(11) || row.getString(11).isEmpty) None else Some(row.getString(11).toDouble),
      row.getString(12)
    )
  }.map(_.asRight[Throwable]).recover {
    case e: Exception => e.asLeft[Entry]
  }.get

  def fromRowUnsafe(row: Row): Entry =
    fromRow(row).getOrElse(Entry(broken = true, "", "", "", "", "", "", "", "", 0, None, "", None, ""))



  val filteredModules =
    Set("10", "20", "99", "3100", "3150", "3250", "3200", "3210", "3280", "3300", "3320", "3321", "3321", "3340", "9000", "9999", "563000", "563001", "563002", "563003", "563004", "563005", "563006", "563007")


  def filterEntry(entry: Entry): Boolean =
    !filteredModules.contains(entry.pnr) && entry.examStatus == "bestanden" && entry.pnr.toInt < 50000


  def findCorrelation(data: Vector[List[Entry]],
                      first: String,
                      second: String,
                      f: Seq[(Int, Int)] => Double): Option[Double] = {
    val relevantPairs =
      data.filter(nel => nel.map(_.examName).contains(first) && nel.map(_.examName).contains(second))
        .traverse(nel => for {
          fst <- nel.find(_.examName == first)
          snd <- nel.find(_.examName == second)
        } yield (fst.points, snd.points))


    relevantPairs.map(f)
  }
}
