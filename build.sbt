name := "ML"

version := "1.0"

scalaVersion := "2.11.12"

scalacOptions += "-J-Xmx4g"


libraryDependencies ++=  Seq(
  "org.apache.spark" %% "spark-core" % "2.1.1",
  "org.apache.spark" %% "spark-mllib" % "2.1.1",
  "eu.timepit" %% "refined" % "0.8.2",
  "com.chuusai" %% "shapeless" % "2.3.3",
  "org.typelevel" %% "cats-core" % "1.0.1",
  "io.circe" %% "circe-core" % "0.9.0",
  "io.circe" %% "circe-generic" % "0.9.0",
  "org.http4s" %% "http4s-dsl" % "0.18.0",
  "org.http4s" %% "http4s-blaze-server" % "0.18.0",
  "org.http4s" %% "http4s-circe" % "0.18.0"
)
